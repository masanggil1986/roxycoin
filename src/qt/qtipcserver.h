#ifndef QTIPCSERVER_H
#define QTIPCSERVER_H

// Define RoxyCoin-Qt message queue name
#define BITCOINURI_QUEUE_NAME "RoxyCoinURI"

void ipcScanRelay(int argc, char *argv[]);
void ipcInit(int argc, char *argv[]);

#endif // QTIPCSERVER_H
